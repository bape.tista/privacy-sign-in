Aplicação de Sign-in Fullstack pensada pela privacidade . 

> desenvolvida com emcima de uma stack chamada [*"Security package"*](https://github.com/stars/97revenge/lists/security-package) que representa dependências de segurança e criptografia . 


### Dependencias : 
- [`kubernetes-client`](https://www.npmjs.com/package/kubernetes-client)



#### requisitos funcionais: 

- integração kubernetes & firebase auth 
- testes com testing library & ts-jest 
