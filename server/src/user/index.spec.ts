import supertest from "supertest";
import express from "express";

import { router } from ".";

const app = express();

app.use(router);

describe("/user validations", () => {
  it("/user HTTP/1.1 200 OK  ", async () => {
    const response = await supertest(app).get("/user/");

    expect(response.status).toBe(200);
  });
});
