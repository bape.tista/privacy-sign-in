import { z } from "zod";

export const userSchema = z.object({
  id: z?.string(),
  name: z.string().min(1).max(30),
  lastName: z.string().min(1).max(70),
  email: z.string().email(),
  password: z.string(),
} satisfies any);

export type typeSchema = z.infer<typeof userSchema>;
