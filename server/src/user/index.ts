import express, { Request, Response, Router } from "express";
import bcrypt from "bcrypt";

import { typeSchema } from "./index.zod";

import bodyParser from "body-parser";
import cors from "cors";

const router: Router = express.Router();
router.use(bodyParser.json());
router.use(cors());

import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

router.get("/user", async (req: Request, res: Response) => {
  const endpoint = req.query.endpoint;
  try {
    const instance: object = await prisma.user.findMany();

    res.send({
      message: "usuario buscado com sucesso",
      content: instance,
      method: endpoint,
      status: res.statusCode,
    });
  } catch (error) {
    console.log(error);
  }
});

router.post("/user", async (req: Request, res: Response) => {
  try {
    const { name, lastName, email, password } = req.body;

    const hashedPassword = await bcrypt.hash(password, 10);

    const instance: typeSchema = await prisma.user.create({
      data: {
        name: name as string,
        lastName,
        email,
        password: hashedPassword,
      },
    });

    res.status(201).send({
      message: "usuario buscado com sucesso",
      content: instance,
      status: res.statusCode,
    });
  } catch (error) {
    console.log(error);
  }
});

export { router };
