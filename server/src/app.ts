import express, { Application } from "express";

import { router as user } from "./user/index";
import cors from "cors";
import bodyParser from "body-parser";

const app: Application = express();
const port = process.env.PORT || 2222;

app.use(user);
app.use(cors());

app.use(express.json());

app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());

app.listen(port, () => console.log("running"));
