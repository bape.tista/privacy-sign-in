import { getAuth, signInWithPopup, GoogleAuthProvider } from "firebase/auth";

export const signInWithGoogle = async () => {
  const auth = getAuth();
  const provider = new GoogleAuthProvider();

  try {
    const instance = await signInWithPopup(auth, provider);

    const credencial = GoogleAuthProvider.credentialFromResult(instance);

    const token = credencial?.accessToken;

    const user = instance.user;

    console.log(JSON.stringify(user));
    return user;
  } catch (err) {
    console.log(err);
  }
};
