import { W, wx } from "windstitch";

const background = wx({
  variants: {
    general: {
      primary: ` p-2 rounded-md flex flex-col items-center`,
    },
    title: {
      primary: `text-3xl  text-gray-90  sm:text-6xl  font-black mb-2`,
    },
    icon: {
      primary: `w-[100px] border border-b-8 text-gray-500 font-custom rounded-md flex flex-row  items-center justify-center gap-2 p-2`,
    },
  },
});

type Props = W.Infer<typeof background>;

const nav: Props = background({
  general: "primary",
});

const title: Props = background({
  title: "primary",
});

const icon: Props = background({
  icon: "primary",
});

export default function Nav({
  name = "usuario",
  connect,
}: {
  name: string;
  connect: React.ReactNode;
}) {
  return (
    <>
      <nav className={nav as string}>
        <h1 className={title as string}>Hey, {name}</h1>

        <div className={icon as string}>
          {/* <p className="mt-1.5 text-md ">Voce está logado pelo </p> */}
          {connect}
        </div>
      </nav>
    </>
  );
}
