import { useState } from "react";
import { Secondary } from "../../components/Form/Gradient";
import { MdiRobotHappy } from "../../components/Icons/MdiRobotHappy";
import Elem from "./Elem";
import Item from "./Item";
import Nav from "./Nav";
import ContentSection from "../Sections/ContentSection";
import { Icon } from "../assets/GoogleIcon";

export default function Content() {
  return (
    <>
      <Secondary>
        <Elem>
          <Item>
            <Nav connect={<Icon />} name="Carlos" />
            <div className=" w-auto flex flex-col items-end p-12">
              <a href="#" className="block shrink-0  ">
                <span className="sr-only">user profile</span>
                <img
                  alt="Man"
                  src="https://images.unsplash.com/photo-1600486913747-55e5470d6f40?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80"
                  className="h-20 w-21 rounded-full object-cover  border-4"
                />
              </a>
            </div>
          </Item>
        </Elem>

        <div className="flex flex-col items-center">
          <div className="relative mx-5  bg-opacity-20 bg-cover bg-center md:max-w-screen-lg  ">
            <img
              className="absolute h-full w-full object-cover rounded-lg"
              src="https://static.nationalgeographicbrasil.com/files/styles/image_3200/public/01_mm9624_210909_12044-edit.jpg?w=1900&h=1425"
            />
            <div className="text-white lg:w-1/2 ">
              <div className=" bg-opacity-95 p-5 opacity-90 backdrop-blur-lg lg:p-12 rounded-lg">
                <p className="mb-4 font-serif font-light">
                  Compartilhe a sua importancia com o mundo
                </p>
                <h2 className=" text-4xl font-bold font-custom">
                  Faca uma pesquisa para saber se o que achamos sobre seguranca
                  na internet
                </h2>
                <a
                  href="#"
                  className="mt-6 inline-block rounded-xl border-2 px-10 py-3 font-semibold border-white hover:bg-white hover:text-blue-600 font-custom"
                >
                  {" "}
                  Faca a pequisa agora{" "}
                </a>
              </div>
            </div>
          </div>
        </div>

        <section className=" py-6 sm:py-8 lg:py-12">
          <div className="mx-auto max-w-screen-xl px-4 md:px-8">
            <div className=" flex  flex-col py-2 w-auto mx-auto mb-10 md:mb-16   rounded-lg border-b-8">
              <h2 className="mb-4 text-center font-custom text-4xl font-bold text-gray-800 md:mb-6 lg:text-3xl">
                Seguranca na internet é coisa séria
              </h2>

              <p className="mx-auto max-w-screen-md text-center text-gray-500 md:text-lg font-custom  bg-white bg-opacity-60 p-2 rounded-lg">
                Segurança online é essencial. Repositórios e posts no Medium
                sobre segurança digital oferecem insights vitais e práticas para
                fortalecer defesas. Educação contínua é chave. Navegar com
                sabedoria protege não apenas indivíduos, mas toda a comunidade
                online, criando um ambiente digital mais seguro.
              </p>
            </div>
            <ContentSection />

            <div className="w-full flex flex-col items-center justify-center p-6  font-custom">
              <p className="text-5xl font-bold border-b-8 border-double">
                todo o conteúdo
              </p>
            </div>
            <div className="grid gap-8 sm:grid-cols-2 sm:gap-12 lg:grid-cols-2 xl:grid-cols-2 xl:gap-16 ">
              <article className="flex flex-col items-center gap-4 md:flex-row lg:gap-6 bg-gray-200 rounded-lg bg-opacity-50 p-2">
                <a
                  href="#"
                  className="group relative block h-56 w-full shrink-0 self-start overflow-hidden rounded-lg bg-gray-100 shadow-lg md:h-24 md:w-24 lg:h-40 lg:w-40"
                >
                  <img
                    src="https://images.unsplash.com/photo-1476362555312-ab9e108a0b7e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
                    loading="lazy"
                    alt=""
                    className="absolute inset-0 h-full w-full object-cover object-center transition duration-200 group-hover:scale-110"
                  />
                </a>

                <div className="flex flex-col gap-2">
                  <span className="text-sm text-gray-400">April 2, 2022</span>

                  <h2 className="text-xl font-bold text-gray-800">
                    <a
                      href="#"
                      className="transition duration-100 hover:text-rose-500 active:text-rose-600"
                    >
                      The Pines and the Mountains
                    </a>
                  </h2>

                  <p className="text-gray-500">
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                    Sint necessitatibus molestias explicabo.
                  </p>

                  <div>
                    <a
                      href="#"
                      className="font-semibold text-rose-500 transition duration-100 hover:text-rose-600 active:text-rose-700"
                    >
                      Read more
                    </a>
                  </div>
                </div>
              </article>{" "}
              <article className="flex flex-col items-center gap-4 md:flex-row lg:gap-6 bg-gray-200 rounded-lg bg-opacity-50 p-2">
                <a
                  href="#"
                  className="group relative block h-56 w-full shrink-0 self-start overflow-hidden rounded-lg bg-gray-100 shadow-lg md:h-24 md:w-24 lg:h-40 lg:w-40"
                >
                  <img
                    src="https://images.unsplash.com/photo-1476362555312-ab9e108a0b7e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
                    loading="lazy"
                    alt=""
                    className="absolute inset-0 h-full w-full object-cover object-center transition duration-200 group-hover:scale-110"
                  />
                </a>

                <div className="flex flex-col gap-2">
                  <span className="text-sm text-gray-400">April 2, 2022</span>

                  <h2 className="text-xl font-bold text-gray-800">
                    <a
                      href="#"
                      className="transition duration-100 hover:text-rose-500 active:text-rose-600"
                    >
                      The Pines and the Mountains
                    </a>
                  </h2>

                  <p className="text-gray-500">
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                    Sint necessitatibus molestias explicabo.
                  </p>

                  <div className=" flex flex-row justify-between p-1 items-center">
                    <a
                      href="#"
                      className="font-semibold text-rose-500 transition duration-100 hover:text-rose-600 active:text-rose-700"
                    >
                      Read more
                    </a>{" "}
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="32"
                      height="32"
                      viewBox="0 0 24 24"
                    >
                      <path
                        fill="currentColor"
                        d="M12 2A10 10 0 0 0 2 12c0 4.42 2.87 8.17 6.84 9.5c.5.08.66-.23.66-.5v-1.69c-2.77.6-3.36-1.34-3.36-1.34c-.46-1.16-1.11-1.47-1.11-1.47c-.91-.62.07-.6.07-.6c1 .07 1.53 1.03 1.53 1.03c.87 1.52 2.34 1.07 2.91.83c.09-.65.35-1.09.63-1.34c-2.22-.25-4.55-1.11-4.55-4.92c0-1.11.38-2 1.03-2.71c-.1-.25-.45-1.29.1-2.64c0 0 .84-.27 2.75 1.02c.79-.22 1.65-.33 2.5-.33c.85 0 1.71.11 2.5.33c1.91-1.29 2.75-1.02 2.75-1.02c.55 1.35.2 2.39.1 2.64c.65.71 1.03 1.6 1.03 2.71c0 3.82-2.34 4.66-4.57 4.91c.36.31.69.92.69 1.85V21c0 .27.16.59.67.5C19.14 20.16 22 16.42 22 12A10 10 0 0 0 12 2Z"
                      />
                    </svg>
                  </div>
                </div>
              </article>
            </div>
          </div>
        </section>
        <footer className="flex  flex-col gap-12 mt-32 px-4 pt-20  items-center justify-center">
          <div className="flex  h-16 w-16   ">
            <MdiRobotHappy />
          </div>

          <p className="py-10 text-center text-gray-500">
            © 2023 | Deus é fiel
          </p>
        </footer>
      </Secondary>
    </>
  );
}
