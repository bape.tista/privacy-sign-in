import { W, wx } from "windstitch";

const background = wx({
  variants: {
    general: {
      primary: `sm:flex sm:items-center sm:justify-between flex  items-center  justify-between bg-gray-100/10 bg-opacity-25 shadow-sm    rounded-md md:py-4  font-custom`,
    },
  },
});

type Props = W.Infer<typeof background>;

const item: Props = background({
  general: "primary",
});

export default function Item({ children }: { children: React.ReactNode }) {
  return (
    <>
      <div className={item as string}>{children}</div>
    </>
  );
}
