import { wx, W } from "windstitch";

const background = wx({
  variants: {
    general: {
      primary: `mx-auto max-w-screen-xl px-4 py-8 sm:px-6 sm:py-12 lg:px-8 `,
    },
  },
});

type Props = W.Infer<typeof background>;

const elem: Props = background({
  general: "primary",
});

export default function Elem({ children }: { children: React.ReactNode }) {
  return (
    <>
      <div className={elem as string}>{children}</div>
    </>
  );
}
