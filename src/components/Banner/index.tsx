import Line from "../../components/Form/Line";
import { Gradient } from "../Form/Gradient";
import {
  HomePagragraph,
  DescriptionParagraph,
} from "../Paragraphs/HomeParagraph";
import ConnectSection from "../Sections/ConnectSection";
import FirestoreSection from "../Sections/FirestoreSection";
import { Elem } from "./Elem";
import { ElemItem } from "./ElemItem";
import { Item } from "./Item";

export default function Banner() {
  return (
    <>
      <Gradient>
        <Elem>
          <ElemItem>
            <Item>
              <HomePagragraph />
              <DescriptionParagraph />
            </Item>

            <FirestoreSection />

            <Line />

            <ConnectSection />
          </ElemItem>
        </Elem>
      </Gradient>
    </>
  );
}
