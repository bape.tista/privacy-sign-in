import { wx, W } from "windstitch";

const background = wx({
  variants: {
    general: {
      primary: `text-center rounded-xl bg-white bg-opacity-20 px-2 py-2`,
    },
  },
});

type Props = W.Infer<typeof background>;

const item: Props = background({
  general: "primary",
});

export const Item = ({ children }: { children: React.ReactNode }) => {
  return (
    <>
      <div className={item as string}>{children}</div>
    </>
  );
};
