import { wx, W } from "windstitch";

const background = wx({
  variants: {
    general: {
      primary: `px-0 mx-auto mt-36 mb-16 max-w-7xl`,
    },
  },
});

type Props = W.Infer<typeof background>;

const elemItem: Props = background({
  general: "primary",
});

export const ElemItem = ({ children }: { children: React.ReactNode }) => {
  return (
    <>
      <div className={elemItem as string}>{children}</div>
    </>
  );
};
