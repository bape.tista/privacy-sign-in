import { wx, W } from "windstitch";

const background = wx({
  variants: {
    general: {
      primary: `h-screen max-h-screen max-w-screen w-screen flex flex-col items-center`,
    },
  },
});

type Props = W.Infer<typeof background>;

const elem: Props = background({
  general: "primary",
});

export const Elem = ({ children }: { children: React.ReactNode }) => {
  return (
    <>
      <div className={elem as string}>{children}</div>
    </>
  );
};
