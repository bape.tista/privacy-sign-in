import { MdiRobotHappy } from "../Icons/MdiRobotHappy";
import IntroParagraph from "../Paragraphs/IntroParagraph";
import namespace from "./Sign.style";
import LoginParagraph from "../Paragraphs/LoginParagraph";
import Modal from "./Modal";

import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { ZodError, z } from "zod";
import { Alert } from "./Alert";

import axios, { AxiosResponse, AxiosError } from "axios";

const userSchema = z.object({
  name: z
    .string()
    .min(1, { message: "Seu nome precisa ter no minimo 1 caractere" })
    .max(30, { message: "seu nome pode ter no maximo 30 caracteres" }),
  lastName: z
    .string()
    .min(1, { message: "Seu sobrenome precisa ter no minimo 1 caractere" })
    .max(70, { message: "Seu sobrenome precisa ter no maximo 60 caracteres" }),
  email: z.string().email({ message: "seu email precisa estar válido" }),
  password: z
    .string()
    .min(8, { message: "sua senha precisa de 8 caracteres" })
    .includes("@", { message: "sua senha precisa de '@' " }),
});

export default function Sign() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: zodResolver(userSchema),
  });

  const onSubmit = async (data: object) => {
    try {
      const instance = await axios
        .post("http://localhost:2222/user?", data)
        .then((response: AxiosResponse) => {
          alert(
            JSON.stringify({
              message: "registrado com sucesso",
              instance: response.data,
              status: response.status,
            })
          );
          if (response.status === 201) {
            window.location.href = "/passed";
          }
        })

        .catch((err: AxiosError) => alert(err));
      return instance;
    } catch (err) {
      if (err instanceof ZodError) {
        console.log(err.issues);
      }
    }
  };

  return (
    <>
      <form
        className={namespace.instanceForm}
        onSubmit={handleSubmit(onSubmit)}
      >
        <MdiRobotHappy />
        <IntroParagraph />
        <label htmlFor="name" className={namespace.instanceLabel}>
          Name
        </label>
        <input
          id="name"
          type="name"
          placeholder="Digite seu Nome"
          className={namespace.instanceInput}
          {...register("name")}
        />
        {errors.name?.message && (
          <Alert children={errors.name?.message as string} />
        )}
        <label htmlFor="lastName" className={namespace.instanceLabel}>
          lastName
        </label>
        <input
          id="lastName"
          type="lastName"
          placeholder="Digite seu Sobrenome"
          className={namespace.instanceInput}
          {...register("lastName")}
        />
        {errors.lastName?.message && (
          <Alert children={errors.lastName?.message as string} />
        )}
        <label htmlFor="email" className={namespace.instanceLabel}>
          email
        </label>
        <input
          id="email"
          type="email"
          placeholder="digite seu email"
          className={namespace.instanceInput}
          {...register("email")}
        />
        {errors.email?.message && (
          <Alert children={errors.email?.message as string} />
        )}
        <label htmlFor="password" className={namespace.instanceLabel}>
          password
        </label>
        <input
          id="password"
          type="password"
          placeholder="Digite sua senha"
          className={namespace.instanceInput}
          {...register("password")}
        />
        {errors.password?.message && (
          <Alert children={errors.password?.message as string} />
        )}
        <Modal />
        <div>
          <button type="submit" className={namespace.instanceButton}>
            Registre-se
          </button>
        </div>
        <LoginParagraph />
      </form>
    </>
  );
}
