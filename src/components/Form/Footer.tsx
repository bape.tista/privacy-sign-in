export default function Footer() {
  return (
    <>
      <div className="flex flex-wrap -mx-3 my-5">
        <div className="w-full max-w-full sm:w-3/4 mx-auto text-center">
          <p className="text-sm text-slate-100 py-1 font-custom">
            Aplicação Fullstack orientada pela privacidade hospedado pelo
            <a
              href="https://gitea.com/bape.tista/privacy-sign-in"
              className="text-slate-700 hover:text-slate-900 font-custom"
              target="_blank"
            >
              Gitea
            </a>{" "}
            Desenvolvida por
            <a
              href="https://linktr.ee/empregos97"
              className="text-slate-700 hover:text-slate-900"
              target="_blank"
            >
              <u>Matheus Pereira</u>
            </a>
            .
          </p>
          <p className="text-sm text-gray-900 py-1 font-custom">
            Saiba mais sobre{" "}
            <a href="https://github.com/stars/97revenge/lists/security-package">
              <u>Secure Package</u>
            </a>
          </p>
        </div>
      </div>
    </>
  );
}
