import { Container } from "./Container";
import { Gradient } from "./Gradient";
import { Elem } from "./Elem";
import { ElemItem } from "./ElemItem";
import { Item } from "./Item";
import Sign from "./Sign";
import Footer from "./Footer";

export default function Form() {
  return (
    <Gradient>
      <Container>
        <Elem>
          <ElemItem>
            <Item>
              <Sign />
            </Item>
          </ElemItem>
        </Elem>
      </Container>
      <Footer />
    </Gradient>
  );
}
