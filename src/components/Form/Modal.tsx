export default function Modal() {
  return (
    <>
      <div className="flex flex-row justify-between mb-8 ">
        <label className="relative inline-flex items-center mr-3 cursor-pointer select-none">
          <input type="checkbox" checked value="" className="sr-only peer" />

          <div className="w-5 h-5 bg-white border-2 rounded-sm border-grey-500 peer peer-checked:border-0 peer-checked:bg-purple-blue-500">
            <img
              className=""
              src="https://raw.githubusercontent.com/Loopple/loopple-public-assets/main/motion-tailwind/img/icons/check.png"
              alt="tick"
            />
          </div>
          <span className="ml-3 text-sm font-normal text-grey-900 font-custom bg-transparent hover:bg-white p-2 rounded-lg">
            Me mantenha logado
          </span>
        </label>
        <a
          href="/"
          className="mr-4 text-sm font-medium text-purple-blue-500 font-custom p-2 hover:bg-white rounded-lg"
        >
          Esqueceu sua senha ?
        </a>
      </div>
    </>
  );
}
