import { type W, wx } from "windstitch";

const background = wx({
  variants: {
    general: {
      primary: `container flex flex-col mx-auto w-full h-full  bg-opacity-40  pt-12 my-5 px-5 `,
    },
  },
});

type Props = W.Infer<typeof background>;

const container: Props = background({
  general: "primary",
});

export const Container = ({ children }: { children: React.ReactNode }) => {
  return (
    <>
      <div className={container as string}>{children}</div>
    </>
  );
};
