import { wx, W } from "windstitch";

const background = wx({
  variants: {
    general: {
      div: `flex items-center mb-3`,
      hr: `h-0 border-b border-solid border-grey-500 grow`,
      p: `mx-4 text-grey-600`,
    },
  },
});

declare type Props<T> = {
  div: T;
  hr: T;
  p: T;
};

const line = {
  div: background({
    general: "div",
  }),
  hr: background({
    general: "hr",
  }),
  p: background({
    general: "p",
  }),
} satisfies Props<W.Infer<typeof background>>;

export default function Line() {
  return (
    <>
      <div className={line.div}>
        <hr className={line.hr} />
        <p className={line.p}>OU</p>
        <hr className={line.hr} />
      </div>
    </>
  );
}
