import { wx, W } from "windstitch";

const background = wx({
  variants: {
    general: {
      div: `w-80 p-3 flex items-center justify-start bg-red-500 rounded-lg shadow-md bg-opacity-80`,
      svg: `w-auto px-3`,
      span: `font-semibold text-sm text-gray-800`,
    },
  },
});

type Props<T> = {
  div: T;
  svg: T;
  span: T;
};

const alert = {
  div: background({
    general: "div",
  }),
  svg: background({
    general: "svg",
  }),
  span: background({
    general: "span",
  }),
} satisfies Props<W.Infer<typeof background>>;

export const Alert = ({ children }: { children: React.ReactNode }) => {
  return (
    <>
      <div className={alert.div}>
        <svg
          fill="none"
          height="24"
          viewBox="0 0 24 24"
          width="24"
          xmlns="http://www.w3.org/2000/svg"
          className={alert.svg}
        >
          <path
            d="m13 14h-2v-5h2zm0 4h-2v-2h2zm-12 3h22l-11-19z"
            fill="#393a37"
          ></path>
        </svg>
        <span className={alert.span}>{children}</span>
      </div>
    </>
  );
};
