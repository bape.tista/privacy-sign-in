import { wx, W } from "windstitch";

const background = wx({
  variants: {
    general: {
      primary: `flex items-center xl:p-10 p-12`,
    },
  },
});

type Props = W.Infer<typeof background>;

const item: Props = background({
  general: "primary",
});

export const Item = ({ children }: { children: React.ReactNode }) => {
  return (
    <>
      <div className={item as string}>{children}</div>
    </>
  );
};
