import { wx, W } from "windstitch";

type NameSpace<T> = {
  form: T;
  label: T;
  input: T;
  button: T;
  span: T;
  instanceForm?: T;
  instanceLabel?: T;
  instanceInput?: T;
  instanceButton?: T;
  instanceSpan?: T;
};

const namespace: NameSpace<any> = {
  form: wx({
    variants: {
      general: {
        primary: `flex flex-col w-full h-full pb-6 text-center  rounded-3xl gap-1 `,
      },
    },
  }),
  label: wx({
    variants: {
      general: {
        primary: `text-xl text-start text-grey-900 font-custom`,
      },
    },
  }),
  input: wx({
    variants: {
      general: {
        primary: `flex items-center w-full px-5 py-4 mr-2 text-sm font-medium outline-none focus:bg-grey-400 mb-2 placeholder:text-grey-700   text-dark-grey-900 rounded-2xl `,
      },
    },
  }),
  button: wx({
    variants: {
      general: {
        primary: ` w-full  text-2xl px-6 py-5  font-bold leading-none text-white transition duration-300 md:w-96 rounded-2xl hover:bg-purple-blue-600 focus:ring-4 focus:ring-purple-blue-100 bg-purple-blue-500 font-custom bg-blue-800 bg-opacity-30 hover:bg-opacity-60`,
      },
    },
  }),
  span: wx({
    variants: {
      general: {
        primary: ` bg-white bg-opacity-80 mr-24 rounded-md shadow-md w-[190px] m-1 
        p-1 border-red-900 border-b-8`,
      },
    },
  }),
};

type TypeForm = W.Infer<typeof namespace.form>;
type TypeLabel = W.Infer<typeof namespace.label>;
type TypeInput = W.Infer<typeof namespace.input>;
type TypeButton = W.Infer<typeof namespace.button>;
type TypeSpan = W.Infer<typeof namespace.span>;

const instanceForm: TypeForm = namespace.form({
  general: "primary",
});

const instanceLabel: TypeLabel = namespace.label({
  general: "primary",
});

const instanceInput: TypeInput = namespace.input({
  general: "primary",
});

const instanceButton: TypeButton = namespace.button({
  general: "primary",
});

const instanceSpan: TypeSpan = namespace.span({
  general: "primary",
});

namespace.instanceForm = instanceForm;
namespace.instanceLabel = instanceLabel;
namespace.instanceInput = instanceInput;
namespace.instanceButton = instanceButton;
namespace.instanceSpan = instanceSpan;

export default namespace;
