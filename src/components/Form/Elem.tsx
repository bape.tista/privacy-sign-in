import { wx, W } from "windstitch";

const background = wx({
  variants: {
    general: {
      primary: `flex justify-center  h-full my-auto xl:gap-14 lg:justify-normal md:gap-5 draggable bg-gray-300 bg-opacity-70 mb-20 mx-2 rounded-lg shadow-lg`,
    },
  },
});

type Props = W.Infer<typeof background>;

const elem: Props = background({
  general: "primary",
});

export const Elem = ({ children }: { children: React.ReactNode }) => {
  return (
    <>
      <div className={elem as string}>{children}</div>
    </>
  );
};
