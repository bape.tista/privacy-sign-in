import { wx, W } from "windstitch";

const background = wx({
  variants: {
    general: {
      primary: `flex items-center justify-center w-full  lg:p-12`,
    },
  },
});

type Props = W.Infer<typeof background>;

const elemItem: Props = background({
  general: "primary",
});

export const ElemItem = ({ children }: { children: React.ReactNode }) => {
  return (
    <>
      <div className={elemItem as string}>{children}</div>
    </>
  );
};
