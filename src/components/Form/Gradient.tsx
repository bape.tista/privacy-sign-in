import { type W, wx } from "windstitch";

const background = wx({
  variants: {
    color: {
      primary: `bg-[conic-gradient(at_bottom,_var(--tw-gradient-stops))] from-lime-200/25 via-lime-500 to-blue-900/20`,
      secondary: `bg-[conic-gradient(at_bottom,_var(--tw-gradient-stops))] from-white via-lime-200 to-white`,
    },
    rounded: {
      small: `rounded-sm`,
      medium: `rounded-lg`,
      large: `rounded-lg`,
    },
    padding: {
      py: `py-2`,
    },
  },
});

type Props = W.Infer<typeof background>;

const gradient: Props | string = String(
  background({
    color: "primary",
    padding: "py",
  })
);
const secondary: Props | string = String(
  background({
    color: "secondary",
    padding: "py",
  })
);

export const Gradient = ({ children }: { children: React.ReactNode }) => {
  return (
    <>
      <div className={gradient as string}>{children}</div>
    </>
  );
};

export const Secondary = ({ children }: { children: React.ReactNode }) => {
  return (
    <>
      <div className={secondary as string}>{children}</div>
    </>
  );
};
