import { wx, type W } from "windstitch";

const background = wx({
  variants: {
    general: {
      primary: `text-sm leading-relaxed text-grey-900`,
      secondary: `font-bold text-grey-700 font-custom underline`,
    },
  },
});

type Props<T> = {
  primary: T;
  secondary: T;
};

const loginParagraph = {
  primary: background({
    general: "primary",
  }),
  secondary: background({
    general: "secondary",
  }),
} satisfies Props<W.Infer<typeof background>>;

export default function LoginParagraph() {
  return (
    <>
      <p className={loginParagraph.primary}>
        Já é registrado ? <br />
        <a href="/" className={loginParagraph.secondary}>
          Faça Login
        </a>
      </p>
    </>
  );
}
