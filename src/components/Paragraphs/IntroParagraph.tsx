import { wx, W } from "windstitch";

const background = wx({
  variants: {
    general: {
      primary: `mb-4 text-grey-700 font-custom font-bold w-auto  text-lg border-b-8  border-blue-200 bg-gray-200 rounded-md lg:w-[500px] lg:p-5 `,
      secondary: `text-5xl mb-2 p-1 font-custom font-bold bg-gray-200 bg-opacity-25 rounded-md`,
    },
  },
});

type Props = W.Infer<typeof background>;

const intoParagraph: Props = background({
  general: "primary",
});

const privacy: Props = background({
  general: "secondary",
});

export default function IntroParagraph() {
  return (
    <>
      <p className={privacy as string}>Privacy</p>
      <p className={intoParagraph as string}>
        Para encontrar a verdadeira liberdade, primeiro temos que entender que{" "}
        <u>'segurança em primeiro lugar'</u> não significa evitar riscos, mas
        sim <u>gerenciá-los com sabedoria</u> .
      </p>
    </>
  );
}
