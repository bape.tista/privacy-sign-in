import { wx, W } from "windstitch";

const background = wx({
  variants: {
    general: {
      h1: `text-5xl font-extrabold tracking-tight text-gray-200 sm:text-5xl md:text-6xl font-title`,
      application: `w-auto h-auto font-custom text-gray-500/90 pr-2`,
      fullstack: `font font-custom  w-[150px] bg-gray-200  bg-opacity-50 rounded-lg text-black/70`,
      projected: `block pt-2 font-custom text-gray-500/90`,
      privacy: `bg-gray-200 bg-opacity-50 text-black/70 rounded-lg`,
    },
  },
});

type Props<T> = {
  h1: T;
  application: T;
  fullstack: T;
  projected: T;
  privacy: T;
};

const homePagragraph = {
  h1: background({
    general: "h1",
  }),
  application: background({
    general: "fullstack",
  }),
  fullstack: background({
    general: "fullstack",
  }),
  projected: background({
    general: "projected",
  }),
  privacy: background({
    general: "privacy",
  }),
} satisfies Props<W.Infer<typeof background>>;

export const HomePagragraph = () => {
  return (
    <>
      <h1 className={homePagragraph.h1 as string}>
        <span className={homePagragraph.application as string}>aplicação</span>{" "}
        <span className={homePagragraph.fullstack as string}>Fullstack </span>
        <span className={homePagragraph.projected}>
          projetada{" "}
          <span className={homePagragraph.privacy}>pela privacidade</span>
        </span>
      </h1>
    </>
  );
};

const _background = wx({
  variants: {
    general: {
      primary: `w-full h-full justify-center   bg-gray-100/10   rounded-md md:py-4  font-custom`,
      secondary: `max-w-md mx-auto mt-3 text-base text-black font-bold sm:text-lg bg-white p-2 bg-opacity-40`,
    },
  },
});

type _Props<T> = {
  primary: T;
  secondary: T;
};

const descriptionParagraph = {
  primary: _background({
    general: "primary",
  }),
  secondary: _background({
    general: "secondary",
  }),
} satisfies _Props<W.Infer<typeof _background>>;

export const DescriptionParagraph = () => {
  return (
    <>
      <div className={descriptionParagraph.primary as string}>
        <p className={descriptionParagraph.secondary as string}>
          Nossa aplicação Fullstack é uma combinação de segurança na internet,
          privacidade e inovação. Integrando{" "}
          <a href="https://www.npmjs.com/package/kubernetes-client">
            <code className="hover:underline">kubernetes-client</code>
          </a>{" "}
          e{" "}
          <a href="https://firebase.google.com/products-build?hl=pt-br">
            <code className="hover:underline">firebase</code>
          </a>
          , realizamos testes abrangentes com a testing library e ts-jest,
          enquanto nosso{" "}
          <a href="https://github.com/stars/97revenge/lists/security-package">
            {" "}
            <u>'Security package'</u>
          </a>{" "}
          <span className="hover:underline">
            garante a proteção de dados. Esta aplicação é uma ótima referencia
            em conexoes confiáveis de dados .
          </span>
        </p>
      </div>
    </>
  );
};
