import { W, wx } from "windstitch";

const background = wx({
  variants: {
    general: {
      one: `flex items-center flex-col gap-2 justify-center p-3 shadow-lg w-auto mt-2  bg-gray-100/10  hover:backdrop-blur-xl   backdrop-blur-lg rounded-md h-[120px]`,
      two: `flex  items-center justify-center bg-white/50 w-auto rounded-md  px-12`,
      three: `flex flex-col items-center justify-center bg-white/50 w-auto rounded-md    bg-white px-8 bg-opacity-50 `,
      four: `text-4xl font-custom font-bold bg-white px-8 bg-opacity-50 hover:underline`,
      five: `text-4xl font-custom font-bold bg-white px-8 bg-opacity-50 hover:underline`,
    },
  },
});

type Props<T> = {
  one: T;
  two: T;
  three: T;
  four: T;
};

const connectSection = {
  one: background({
    general: "one",
  }),
  two: background({
    general: "two",
  }),
  three: background({
    general: "three",
  }),
  four: background({
    general: "four",
  }),
} satisfies Props<W.Infer<typeof background>>;

export default function ConnectSection() {
  return (
    <>
      <div className={connectSection.one}>
        <div className=" flex  items-center justify-center bg-white/50 w-auto rounded-md  px-12  ">
          <a
            href="/sign"
            className="text-4xl font-custom font-bold bg-white px-8 bg-opacity-50 hover:underline"
          >
            Registre-se{" "}
          </a>{" "}
        </div>{" "}
        <div className=" flex flex-col items-center justify-center bg-white/50 w-auto rounded-md    bg-white px-8 bg-opacity-50  ">
          <a
            href="/login"
            className="text-4xl font-custom font-bold bg-white px-8 bg-opacity-50 hover:underline"
          >
            Faca-login{" "}
          </a>
        </div>
      </div>
    </>
  );
}
