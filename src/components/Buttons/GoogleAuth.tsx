import { wx, W } from "windstitch";
import { signInWithGoogle } from "../../features/firebase/firebase.sign";

const background = wx({
  variants: {
    general: {
      elem: `flex w-auto  p-2  justify-center`,
      anchor: `flex items-center justify-center w-[250px] py-4 mb-6 text-sm font-medium transition duration-300 rounded-3xl text-grey-900 bg-grey-300 hover:bg-grey-400 focus:ring-4 focus:ring-grey-300 bg-white`,
      image: `h-5 mr-2`,
    },
  },
});

type Props<T> = {
  elem: T;
  anchor: T;
  image: T;
};

const googleAuth = {
  elem: background({
    general: "elem",
  }),
  anchor: background({
    general: "anchor",
  }),
  image: background({
    general: "image",
  }),
} satisfies Props<W.Infer<typeof background>>;

export default function GoogleAuth() {
  return (
    <>
      <div className={googleAuth.elem}>
        <button
          className={googleAuth.anchor}
          type="submit"
          onSubmit={signInWithGoogle}
        >
          <img
            className={googleAuth.image}
            src="https://raw.githubusercontent.com/Loopple/loopple-public-assets/main/motion-tailwind/img/logos/logo-google.png"
            alt=""
          />
          Conecte-se com o Google
        </button>
      </div>
    </>
  );
}
