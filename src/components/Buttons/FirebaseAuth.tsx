import { ReactNode } from "react";

export default function FirebaseAuth({
  children,
  click,
}: {
  children: ReactNode;
  click?: any;
}) {
  return (
    <>
      <button
        onClick={click}
        className="flex items-center justify-center p-3 shadow-lg w-full  bg-gray-100/10  hover:backdrop-blur-xl hover:bg-white  backdrop-blur-lg rounded-md cursor-pointer  "
      >
        {children}
      </button>
    </>
  );
}
