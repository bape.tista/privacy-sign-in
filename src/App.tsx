import { Route, Routes, BrowserRouter } from "react-router-dom";

import Home from "./screens/home";
import Sign from "./screens/sign";
import Login from "./screens/login";
import Feed from "./screens/feed";
import Test from "./screens/test";

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/sign" element={<Sign />} />
          <Route path="/login" element={<Login />} />
          <Route path="/feed/:id" element={<div> thats ok </div>} />
          <Route path="/feed/" element={<Feed />} />
          <Route path="/test/" element={<Test />} />
          <Route path="/404/" errorElement={<div> erro 404</div>} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
