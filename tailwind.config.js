module.exports = {
  theme: {
    extend: {
      screens: {
        sm: "640px",
        md: "768px",
        lg: "1024px",
        xl: "1280px",
      },
      fontFamily: {
        custom: ["Barlow Condensed", "sans-serif"],
      },
      babel: {
        presets: ["@emotion/babel-preset-css-prop"],
      },
    },
  },
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  plugins: ["prettier-plugin-tailwindcss"],
};
